package com.example.api;

import android.content.SyncRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("palabra")
    @Expose
    private String palabra;

    public User(String id, String palabra){
        this.palabra = palabra;
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", palabra='" + palabra + '\'' +
                '}';
    }
}
