package com.example.api;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Lista extends AppCompatActivity{

    UserService reportService;

    private ListView listView;
    private  ArrayList<String> denuncia;
    private ArrayAdapter<String> adapter;
    public static final String EXTRA_MESSAGE = "app.activities.MESSAGE";

    public static String USERID = "";
    public static String CursoIdCreado = "";
    DatabaseHelper db = new DatabaseHelper(Lista.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //OBTENER Token DEL USUARIO QUE TRAIGO DEL LOGI

        reportService = Connection.getServiceRemotee();
        //System.out.println(Login.user_key);

        listView = findViewById(R.id.ViewList);
        denuncia = new ArrayList<>();

        adapter= new ArrayAdapter<String>(Lista.this, R.layout.listalayout, R.id.txtReporte, denuncia);
        listView.setAdapter(adapter);
        //get
        Call<ResponseBody> call = reportService.get();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String[] datos;
                    String [] dato;
                    ArrayList<String> data = new ArrayList<>();
                    datos = response.body().string().split(":",2);
                    datos[0] = "";
                    for(int i = 0; i<datos[1].length(); i++){
                        if(datos[1].charAt(i) != '{' && datos[1].charAt(i) != '}' &&
                                datos[1].charAt(i) != '[' && datos[1].charAt(i) != ']' &&
                                datos[1].charAt(i) != '"' && datos[1].charAt(i) != 92){
                            datos[0] += datos[1].charAt(i);
                        }
                    }
                    datos = datos[0].split(",");
                    System.out.println(datos[1]);
                    if (response.code() == 200) {
                        for(String i : datos){
                            System.out.println(i);
                            dato = i.split(":");
                            if (dato[0].equals("id")){
                                data.add(dato[1]);
                            }else {
                                data.set(data.size()-1, data.get(data.size()-1) + ":" + dato[1]);
                                System.out.println(data);
                            }
                        }
                        for(String i : data){
                            dato = i.split(":");
                            System.out.println(dato);
                            CursoIdCreado = dato[0];
                            denuncia.add(dato[1]);
                            adapter.notifyDataSetChanged();
                        }
                    }

                }catch (IOException e){}//ram@alv.com

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Error " + t);
            }
        });
    }
}



