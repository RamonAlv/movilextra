package com.example.api;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity{

    UserService userService;
    DatabaseHelper dbHandler = new DatabaseHelper(Login.this);
    public static final String EXTRA_MESSAGE = "app.activities.MESSAGE";

    private EditText palabra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         palabra = findViewById(R.id.email);
        final Button register = findViewById(R.id.btnRegister);

        userService = Connection.getServiceRemotee();

    }

    public void OnPalabra(View v){
        String word = palabra.getText().toString().toLowerCase();
        boolean BanA = false, BanE = false, BanI = false, BanO = false, BanU = false;
        for(int i = 0; i<word.length();i++){
            if(word.charAt(i) == 'a' || word.charAt(i) == 'á'){
                BanA = true;
                System.out.println(word.charAt(i));
            }

            else if(word.charAt(i) == 'e' || word.charAt(i) == 'é') {
                BanE = true;
                System.out.println(word.charAt(i));
            }
            else if(word.charAt(i) == 'i' || word.charAt(i) == 'í') {
                BanI = true;
                System.out.println(word.charAt(i));
            }
            else if(word.charAt(i) == 'o' || word.charAt(i) == 'ó') {
                BanO = true;
                System.out.println(word.charAt(i));
            }
            else if(word.charAt(i) == 'u' || word.charAt(i) == 'ú') {
                BanU = true;
                System.out.println(word.charAt(i));
            }
        }
        if (BanA && BanE && BanI && BanO && BanU){
            Call<ResponseBody> call = userService.palabra(word);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        Toast.makeText(getApplicationContext(), "Palabra vovcablos agregada", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    System.out.println("Error =>" + t);
                }
            });
        }else
            Toast.makeText(getApplicationContext(), "Palabra no vovcablos", Toast.LENGTH_SHORT).show();

    }

}
