package com.example.api;

import android.graphics.Bitmap;

import java.io.File;
import java.nio.file.FileVisitResult;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ReportService {

    @GET("cursos/creados/{id}")
    Call<ResponseBody> getCursos(
            @Path("id") String id
    );

    @Multipart
    @POST("imagenu")
    Call<ResponseBody> addReport(
            @Part MultipartBody.Part image,
            @Part("image") RequestBody name
    );

    @GET("reports/{id}")
    Call<ResponseBody> getByIdReport(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth,
            @Path("id") String id
    );

    @GET
    Call<ResponseBody> getImg(
            @Url String URL
    );
    @PUT("reports/{id}")
    Call<Report> updateReport(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth,
            @Path("id") String id,
            @Body Report report
            );

    @DELETE("reports/{id}")
    Call<ResponseBody> deleteReport(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth,
            @Path("id") String id
    );

    @GET("logout")
    Call<ResponseBody> logout(
            @Header("Content-Type") String Type,
            @Header("Authorization") String Auth
    );
}
