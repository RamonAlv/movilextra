package com.example.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Report{

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("fecha")
    @Expose
    private String fecha;

    @SerializedName("costo")
    @Expose
    private String costo;

    @SerializedName("entradas")
    @Expose
    private String entradas;

    @SerializedName("status")
    @Expose
    private String status;

    public Report(){};

    public Report(String id, String user_id, String name, String description, String fecha, String costo, String entradas, String status){
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.description = description;
        this.fecha = fecha;
        this.costo = costo;
        this.entradas = entradas;
        this.status = status;
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getEntradas() {
        return entradas;
    }

    public void setEntradas(String entradas) {
        this.entradas = entradas;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", fecha='" + fecha + '\'' +
                ", costo='" + costo + '\'' +
                ", entradas='" + entradas + '\'' +
                '}';
    }
}
