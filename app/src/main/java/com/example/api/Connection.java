package com.example.api;

public class Connection {
    private Connection(){};
    public static  String API_URL = "http://192.168.43.225:8000/api/auth/";

    public static ReportService getServiceRemote(){
        return Client.getClient(API_URL).create(ReportService.class);
    }
    public static UserService getServiceRemotee(){
        return Client.getClient(API_URL).create(UserService.class);
    }
}
